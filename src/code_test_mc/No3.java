/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code_test_mc;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class No3 {
    
    /*
    Example Operation
    Enter string : souvenir loud four lost

    Enter value of X : 4

    Result : 
    loud four lost 
    */
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter string : ");
        String input = sc.nextLine();

        System.out.print("\nEnter value of X : ");
        int x = sc.nextInt();
        
        String[] arr = input.split(" ");
        System.out.println("\nResult : ");
        Arrays.stream(arrOperation(arr,x))
                      .filter(item -> (item != null))
                      .forEach(item -> System.out.print(item + " "));
        System.out.println("");
    }
    
    private static String[] arrOperation(String[] arr, int x){      
        return Arrays.stream(arr)
              .filter(item -> (item.length() == x))
              .toArray(String[]::new);
    }
}
