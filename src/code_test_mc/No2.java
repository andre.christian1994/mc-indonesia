/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code_test_mc;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class No2 {
    /*
    Example operation
    
    Enter size of array : 4

    Enter numbers :  1 2 3 4

    Enter value of X : 4

    Result : 
    1 2 3 
    */
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size of array : ");
        int arrSize = sc.nextInt();
        System.out.print("\nEnter numbers : ");
        
        Integer[] arr = new Integer[arrSize];
        for(int i = 0 ; i < arr.length; i ++){
            arr[i] = sc.nextInt();
        }
        
        System.out.print("\nEnter value of X : ");
        int x = sc.nextInt();
        
        System.out.println("\nResult : ");
        Arrays.stream(arrOperation(arr,x))
                      .filter(item -> (item != null))
                      .forEach(item -> System.out.print(item + " "));
        System.out.println("");
    }
    
    private static Integer[] arrOperation(Integer[] arr, int x){
        Arrays.sort(arr);
        Integer[] result = new Integer[arr.length];
        for(int i = 0 ; i < arr.length ; i ++){
            boolean isEqualX = false;
            for (Integer arr1 : arr) {
                if (arr[i] / arr1 == x) {
                    isEqualX = true;              
                }
            }
            if(!isEqualX){
                result[i] = arr[i];
            } else{
                result[i] = null;
            }
        }
        return result;
    }
}
