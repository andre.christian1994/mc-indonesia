/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code_test_mc;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class No1 {
    
    /*
        Example Operation
    
        Enter size of array : 4

        Enter numbers : 3 1 4 2

        Result : 
        4 
    */ 
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter size of array : ");
        int arrSize = sc.nextInt();
        System.out.print("\nEnter numbers : ");
        
        int[] arr = new int[arrSize];
        for(int i = 0 ; i < arr.length; i ++){
            arr[i] = sc.nextInt();
        }
        
        System.out.println("\nResult : ");
        Arrays.stream(arrOperation(arr))
                      .forEach(item -> System.out.print(item + " "));
        System.out.println("");
    }
    
    private static int[] arrOperation(int[] arr){
        Arrays.sort(arr);
        return Arrays.stream(arr)
                .filter(item -> (item == arr[arr.length-1]))
                .toArray();
    }
}
